
template <class T>
Conjunto<T>::Conjunto() {
    _raiz = nullptr;
    _cardinal = 0;
}

template <class T>
Conjunto<T>::~Conjunto() { 
    while(this->_cardinal > 0){
        this->remover(_raiz->valor);
    }
    _raiz= nullptr;
    return;
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* iterador = _raiz;

    while (iterador != nullptr && iterador->valor != clave){
        if(clave < iterador->valor){
            iterador=iterador->izq;
        } else {
            iterador = iterador->der;
        }
    }
    return iterador != nullptr;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if (this->pertenece(clave)){}

    else if(this->cardinal() == 0){
        struct Nodo *elemNuevo = new Nodo;
        elemNuevo->valor=clave;
        elemNuevo->der= nullptr;
        elemNuevo->izq= nullptr;
        elemNuevo->padre = nullptr;

        _raiz = elemNuevo;
        _cardinal ++;
    } else {
        //Tengo mas de un valor y tengo que recorrer
        Nodo *iterador = _raiz;
        Nodo *elemNuevo = new Nodo;
        elemNuevo->valor = clave;
        elemNuevo->der = nullptr;
        elemNuevo->izq = nullptr;

        bool parar = false;
        while (!parar) {
            if (clave < iterador->valor) {
                if(iterador->izq == nullptr){
                    parar = true;
                    iterador->izq = elemNuevo;
                    elemNuevo->padre=iterador;
                } else {
                    iterador = iterador->izq;
                }
            } else {
                if (iterador->der == nullptr) {
                    parar = true;
                    iterador->der = elemNuevo;
                    elemNuevo->padre=iterador;
                } else {
                    iterador = iterador->der;
                }
            }
        }
        _cardinal ++;
    }
    return;
}

template <class T>
void Conjunto<T>::remover(const T& clave) { //asumo que clave pertenece
    if (_cardinal == 1) {
        struct Nodo* punt = _raiz;
        _raiz = nullptr;
        delete punt;
        _cardinal--;
        //PEPINO

    } else{
        //Parar a iterador sobre el nodo a borrar, y a papaIterador sobre su padre
        struct Nodo* iterador = _raiz;

        while (clave != iterador->valor) {
            if(clave < iterador->valor){
                iterador = iterador->izq;
            }
            else {
                iterador = iterador->der;
            }
        }
        Nodo* papaIterador = iterador->padre;

        //me fijo si soy una hoja (No puedo ser una hoja y la raiz a la vez(1er if))
        if(iterador->izq == nullptr && iterador->der == nullptr){

            if(papaIterador->izq == iterador){
                //iterador es el hijo izquierdo de papaIterador
                papaIterador->izq = nullptr;
                delete iterador;
            }
            else {
                //iterador es el hijo derecho de papaIterador
                papaIterador->der = nullptr;
                delete iterador;
            }
            _cardinal--;

        } else if(iterador->izq == nullptr){ // me fijo si solo tengo un hijo derecho
            if(iterador == _raiz){ //Caso especial para la raiz
                _raiz = iterador->der;
                delete iterador;
            } else if(papaIterador->izq == iterador) { //caso soy hijo izquierdo y tengo hijo der
                papaIterador->izq = iterador->der;
                iterador->der->padre = papaIterador;
                delete iterador;
            } else {
                papaIterador->der = iterador->der; // caso soy hijo der y tengo hijo der
                iterador->der->padre = papaIterador;
                delete iterador;
            }
            _cardinal--;

        } else if(iterador->der == nullptr){ // me fijo si solo tengo un hijo izquierdo
            if (iterador == _raiz){ //caso especial para la raiz
                _raiz = iterador->izq;
                delete iterador;
            }else if (papaIterador->izq == iterador){ //caso soy hijo izq y tengo hijo izq
                papaIterador->izq = iterador->izq;
                iterador->izq->padre = papaIterador;
                delete iterador;
            } else { //caso soy hijo der y tengo hijo izq
                papaIterador->der = iterador->izq;
                iterador->izq->padre = papaIterador;
                delete iterador;
            }
            _cardinal--;

        } else { //caso dos hijos
            Nodo* reemplazo = iterador->der;
            T valorReemp;

            while (reemplazo->izq != nullptr){
                reemplazo = reemplazo->izq;
            }
            valorReemp = reemplazo->valor;

            remover(valorReemp);
            iterador->valor = valorReemp;

        }
    }
    return;
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* iterador = _raiz;
    while (iterador->valor != clave){
        if(clave < iterador->valor){
            iterador=iterador->izq;
        } else {
            iterador = iterador->der;
        }
    }
    //Paro a iterador sobre el elemento al que le quiero encontrar su siguiente

    Nodo* sig = iterador;
    if (iterador->der != nullptr){ //caso tengo hijo derecho
        sig = iterador->der;
        while (sig->izq != nullptr){
            sig = sig->izq;
        }
    } else { //caso no tengo hijo derecho
        while (sig->padre->der == sig){ //mientras que seas el hijo derecho
            sig = sig->padre; //subi a tu padre
        }
        sig = sig->padre; //subo por ultima vez
    }
    return sig->valor;

}

template <class T>
const T& Conjunto<T>::minimo() const {
    if (_cardinal == 0) {
        return 0;
    } else {

        Nodo *iterador = _raiz;
        while (iterador->izq != nullptr) {
            iterador = iterador->izq;
        }
        return iterador->valor;
    }
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* iterador = _raiz;
    while (iterador->der != nullptr){
        iterador = iterador->der;
    }
    return iterador->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

