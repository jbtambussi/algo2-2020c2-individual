#include <iostream>
#include <list>
using namespace std;

// Pre: 0 <= mes < 12
// Copio Recordatorios.h a .cpp

class Fecha {
public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();

    bool operator==(const Fecha o) const ;
    bool operator<(const Fecha o) const ;
    bool operator>(const Fecha o) const ;

private:
    int mes_;
    int dia_;
};

class Horario {
public:
    Horario(uint hora, uint minuto);
    uint hora();
    uint min();

    bool operator<(const Horario h) const ;
    bool operator>(const Horario h) const ;

private:
    uint hora_;
    uint min_;
};

class Recordatorio {
public:
    Recordatorio(Fecha f, Horario h, string evento);
    Fecha fecha();
    Horario horario();
    string evento();

    bool operator>(const Recordatorio r) const ;
    bool operator<(const Recordatorio r) const ;

private:
    Fecha fecha_;
    Horario horario_;
    string evento_;
};


class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();


private:
    Fecha fechaActual_;
    map<Fecha, list<Recordatorio>> recor_;
};

uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia) {}

int Fecha::mes() {return mes_;}
int Fecha::dia() {return dia_;}
ostream& operator<<(ostream& os, Fecha fecha){
    os << fecha.dia() << "/" << fecha.mes();
    return os;
}
bool Fecha::operator==(const Fecha o) const {
    return (dia_ == o.dia_ && mes_ == o.mes_);
}
bool Fecha::operator<(const Fecha o) const {
    return (mes_ < o.mes_ || (mes_ == o.mes_ && dia_ < o.dia_));
}
bool Fecha::operator>(const Fecha o) const {
    return (mes_ > o.mes_ || (mes_ == o.mes_ && dia_ > o.dia_));
}

void Fecha::incrementar_dia() {
    if(dia_ == dias_en_mes(mes_))
    {
        dia_ = 1;
        mes_ ++;
    } else{
        dia_++;
    }
}


//Clase horario

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min){}
uint Horario::hora() {return hora_;}
uint Horario::min() {return min_;}

ostream& operator<<(ostream& os, Horario horario){
    os << horario.hora() << ":" << horario.min();
    return os;
}

bool Horario::operator<(const Horario h) const {
    return (this->hora_ < h.hora_ || (hora_ == h.hora_ && this->min_ < h.min_));
}
bool Horario::operator>(const Horario h) const {
    return (this->hora_ > h.hora_ || (hora_ == h.hora_ && this->min_ > h.min_));
}


// Clase Recordatorio

Recordatorio::Recordatorio(Fecha f, Horario h, string evento) : fecha_(f), horario_(h), evento_(evento){}

string Recordatorio::evento() {return evento_;}
Fecha Recordatorio::fecha() {return fecha_;}
Horario Recordatorio::horario() {return horario_;}

ostream& operator<<(ostream& os, Recordatorio rec){
    os << rec.evento() << " @ " << rec.fecha() << " " << rec.horario();
    return os;
}
bool Recordatorio::operator>(const Recordatorio r) const {
    return (fecha_<r.fecha_ || (fecha_ == r.fecha_ && horario_<r.horario_));
}
bool Recordatorio::operator<(const Recordatorio r) const {
    return (fecha_>r.fecha_ || (fecha_ == r.fecha_ && horario_>r.horario_));
}

//Clase Agenda

Agenda::Agenda(Fecha fechaInicial) :fechaActual_(fechaInicial){}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    if(recor_.count(rec.fecha())==0){
        recor_[rec.fecha()] = list<Recordatorio>(); //No estoy seguro de si es necesario este paso
    }
    recor_[rec.fecha()].push_front(rec);
    return;
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> listaADevolver = recor_[fechaActual_];
    listaADevolver.sort();
    return listaADevolver;
}


void Agenda::incrementar_dia() {
    fechaActual_.incrementar_dia();
    return;
}

ostream& operator<<(ostream& os, Agenda a) {
    list<Recordatorio> recordAImprimir = a.recordatorios_de_hoy();
    os << a.hoy() << endl << "=====" << endl;
    for(int i = 0; i < a.recordatorios_de_hoy().size(); i++){
        os << recordAImprimir.back() << endl;
        recordAImprimir.pop_back();
    }
    return os;
}

Fecha Agenda::hoy() {return fechaActual_;}