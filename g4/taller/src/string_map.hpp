
#include "string_map.h"

template <typename T>
string_map<T>::string_map(){
    _raiz = nullptr;
    _size = 0;
    _nodosDef = vector<Nodo*>();
    // COMPLETAR
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.



template <typename T>
string_map<T>::~string_map(){
    while (_nodosDef.size() != 0){
        this->erase(_nodosDef.back()->clave);
    }
}

template <typename T>
T& string_map<T>::operator[](const string& clave){ //NO IMPLEMENTADO
    // COMPLETAR
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    if(_size == 0){
        return 0;
    }

    Nodo* iterador = _raiz;
    int c = 0;
    // Me muevo hasta donde puedo en el camino hacia clave
    while(iterador->clave != clave && pertASig(iterador->siguientes, clave.substr(0,c+1))){
        int k = 0;
        while (iterador->siguientes[k]->clave != clave.substr(0,c+1)){
            k++;
        }
        c++;
        iterador = iterador->siguientes[k];
    }

    // Me fijo si llegue a la clave y si llegue, que haya sido definida.
    if(iterador->clave == clave && iterador->esDef){
        return 1;
    }
    else {
        return 0;
    }
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    //copie y pegue codigo de abajo

    Nodo* iterador = _raiz;
    int c = 0;
    while(iterador->clave != clave){
        int k = 0;
        while (iterador->siguientes[k]->clave != clave.substr(0,c+1)){
            k++;
        }
        c++;
        iterador = iterador->siguientes[k];
    }
    return (iterador->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* iterador = _raiz;
    int c = 0;

    //me paro sobre clave
    while(iterador->clave != clave){
        int k = 0;
        while (iterador->siguientes[k]->clave != clave.substr(0,c+1)){
            k++;
        }
        c++;
        iterador = iterador->siguientes[k];
    }
    return (iterador->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* actual = _raiz;
    int c = 0;
    // me paro sobre clave
    while(actual->clave != clave){
        int k = 0;
        while (actual->siguientes[k]->clave != clave.substr(0,c+1)){
            k++;
        }
        c++;
        actual = actual->siguientes[k];
    }

    actual->esDef = false;

    for (int i = 0; i < _nodosDef.size(); i++){
        if (_nodosDef[i] == actual) {
            _nodosDef.erase(_nodosDef.begin() + i);
        }
    }


    // La borro de la lista privada con todos los nodos definidos
    //me fijo si tiene algun hijo, asegurandome que no sea la raiz(creo que la parte de no raiz es redundante)
    if(actual->siguientes.size() == 0 && actual->padre != nullptr) {
        //borro el nodo de clave
        Nodo *aBorrar = actual;
        actual = aBorrar->padre;
        for (int i = 0; i < actual->siguientes.size(); i++) {
            if (actual->siguientes[i] == aBorrar) {
                vector<Nodo *> nod = actual->siguientes;
                nod.erase(nod.begin() + i);
                actual->siguientes = nod;
            }
        }
        delete aBorrar;

        // voy subiendo y borro todos los nodos que queden sin hijos(mientras que no sean la raiz)
        while (actual->padre != nullptr && actual->siguientes.size() == 0 && !actual->esDef){
            aBorrar = actual;
            actual = actual->padre;
            for(int i = 0; i < actual->siguientes.size(); i++) {
                if (actual->siguientes[i] == aBorrar) {
                    vector<Nodo*> nod = actual->siguientes;
                    nod.erase(nod.begin() + i);
                    actual->siguientes = nod;
                }
            }
            delete aBorrar;
        }


    } else {
        actual->esDef = false;
    }

    _size--;

    // Si no queda elementos entonces el unico nodo que queda es la raiz, la borro
    if(_size == 0){
        delete _raiz;
        _raiz = nullptr;
    }
    return;
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return (_size == 0);
}

template<typename T>
void string_map<T>::insert(const pair<string, T>& tupla) {
    if(_size == 0){
        Nodo* nodoRaiz = new Nodo;
        nodoRaiz->siguientes.clear();
        nodoRaiz->clave = "";
        nodoRaiz->padre = nullptr;
        nodoRaiz->esDef = false;
        _raiz = nodoRaiz;
    }
    Nodo* iterador = _raiz;

    for(int i = 0; i<tupla.first.size() - 1; i++){ //voy hasta al nodo padre
        if(pertASig(iterador->siguientes, tupla.first.substr(0, i+1))){ //caso que el siguiente nodo a recorrer existe
            int k = 0;
            while (iterador->siguientes[k]->clave != tupla.first.substr(0,i+1)){
                k++;
            }
            iterador = iterador->siguientes[k];
        }
        else { //caso que el nodo siguiente no exista
            //Aca iria el checkeo de borrados
            Nodo* nodoNuevo = new Nodo;
            nodoNuevo->clave = tupla.first.substr(0, i+1);
            nodoNuevo->padre = iterador;
            nodoNuevo->siguientes.clear();
            nodoNuevo->esDef = false;
            iterador->siguientes.push_back(nodoNuevo);
            iterador=nodoNuevo;
        }
    }

    //creacion del nodo interesado
    if(pertASig(iterador->siguientes, tupla.first)){ //existe el nodo
        int k = 0;
        while (iterador->siguientes[k]->clave != tupla.first){
            k++;
        }
        iterador = iterador->siguientes[k];
        if(iterador->esDef){ //Caso el nodo esta definido(redefinir)
            iterador->definicion = tupla.second;
        }
        else { //Caso el nodo no esta definido
            iterador->esDef = true;
            iterador->definicion = tupla.second;
            _nodosDef.push_back(iterador);
            _size ++;
        }
    } else {
        Nodo* nodoNuevo = new Nodo;
        nodoNuevo->clave = tupla.first;
        nodoNuevo->padre = iterador;
        nodoNuevo->siguientes = vector<Nodo*>();
        nodoNuevo->esDef = true;
        iterador->siguientes.push_back(nodoNuevo);
        nodoNuevo->definicion = tupla.second;
        _nodosDef.push_back(nodoNuevo);
        _size ++;
    }

    return;
}

template<typename T>
bool string_map<T>::pertASig(const vector<Nodo *> vect, const string clave) const {
    bool ret = false;
    for(int i = 0; i < vect.size(); i++){
        if (clave == vect[i]->clave){
            ret = true;
        }
    }
    return ret;

}

template<typename T>
void string_map<T>::auxCopiarRec(string_map::Nodo *actual, string_map<T>& s) {
    if(actual->siguientes.size() == 0 && actual->esDef){
        s.insert(make_pair(actual->clave, actual->definicion));
    } else if (actual->esDef) {
        s.insert(make_pair(actual->clave, actual->definicion));
        for(Nodo* n : actual->siguientes){
            auxCopiarRec(n, s);
        }
    } else {
        for(Nodo* n : actual->siguientes){
            auxCopiarRec(n, s);
        }
    }
}



/*
    La funcion presentada a continuacion no funciona, tira un error de segmentation fault
*/
template<typename T>
string_map<T> &string_map<T>::operator=(const string_map &d) {
    //borra todos los nodos que tenia antes

    while (_nodosDef.size() != 0){
        this->erase(_nodosDef.front()->clave);
    }

    //agrega los nodos de d
    vector<Nodo*> vectNodo = d._nodosDef;
    string_map<int> nMapa;

    while(vectNodo.size() != 0){
        this->insert(make_pair(vectNodo.back()->clave,vectNodo.back()->definicion));
        vectNodo.pop_back();
    }

    return nMapa; // No puedo devolver This, justo antes del return o en el return, nMapa se re-inicializa
}

