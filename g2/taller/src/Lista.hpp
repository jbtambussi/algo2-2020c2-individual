#include "Lista.h"

Lista::Lista() {
    longitud_ = 0;
    pPrimElem_ = nullptr;
    pUltElem_ = nullptr;
}

Lista::Lista(const Lista& l) : Lista() {
    for(int i =0; i < l.longitud_; i++){
        this->agregarAtras(l.iesimo(i));
    }
    *this = l;
}

Lista::~Lista() {
    while (longitud() > 0){
        eliminar(0);
    }
    return;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    while (longitud() > 0){
        eliminar(0);
    }
    for(int i =0; i < aCopiar.longitud_; i++){
        this->agregarAtras(aCopiar.iesimo(i));
    }

    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    struct Nodo* pNodoNuevo = new Nodo;
    pNodoNuevo->data = elem;

    // caso lista con 0 elementos
    if(longitud_ == 0){
        pNodoNuevo->prev = nullptr;
        pNodoNuevo->succ = nullptr;

        pPrimElem_ = pNodoNuevo;
        pUltElem_ = pNodoNuevo;
    } else {
        pNodoNuevo->prev = nullptr;
        pNodoNuevo->succ = pPrimElem_;

        pPrimElem_->prev = pNodoNuevo;
        pPrimElem_ = pNodoNuevo;
    }
    longitud_ ++;
    return;
}

void Lista::agregarAtras(const int& elem) {
    struct Nodo* pNodoNuevo = new Nodo;
    pNodoNuevo->data = elem;

    // caso lista con 0 elementos
    if(longitud_ == 0){
        pNodoNuevo->prev = nullptr;
        pNodoNuevo->succ = nullptr;

        pPrimElem_ = pNodoNuevo;
        pUltElem_ = pNodoNuevo;
    } else {
        pNodoNuevo->prev = pUltElem_;
        pNodoNuevo->succ = nullptr;

        pUltElem_->succ = pNodoNuevo;

        pUltElem_ = pNodoNuevo;
    }
    longitud_ ++;
    return;
}

void Lista::eliminar(Nat i) {
    struct Nodo* pNodoTemp = pPrimElem_;
    for(int k = 0; k < i; k++){
        pNodoTemp = pNodoTemp->succ;
    }
    //Estoy en la posicion iesima
    if(longitud() == 1){
        delete pNodoTemp;
        pPrimElem_ = nullptr;
        pUltElem_ = nullptr;
    }

    else if(pNodoTemp->prev == nullptr){ // o i=0
        //El elemento a eliminar es el primero
        pPrimElem_ = pNodoTemp->succ;
        (pNodoTemp->succ)->prev = nullptr;
        delete pNodoTemp;
    }
    else if(pNodoTemp->succ == nullptr){ // o i = longitud_ -1
        //El elemento a eliminar es el ultimo
        pUltElem_ = pNodoTemp->prev;
        (pNodoTemp->prev)->succ = nullptr;
        delete pNodoTemp;
    }
    else {
        //El elemento a eliminar esta en el medio
        pNodoTemp->prev->succ = pNodoTemp->succ;
        pNodoTemp->succ->prev = pNodoTemp->prev;
        delete pNodoTemp;
    }
    longitud_--;
    return;
}

int Lista::longitud() const {
    // Completar
    return longitud_;
}

const int& Lista::iesimo(Nat i) const {
    struct Nodo* pNodoTemp = pPrimElem_;
    for(int k = 0; k < i; k++){
        pNodoTemp = pNodoTemp->succ;
    }
    return(pNodoTemp->data);
}

int& Lista::iesimo(Nat i) {
    struct Nodo* pNodoTemp = pPrimElem_;
    for(int k = 0; k < i; k++){
        pNodoTemp = pNodoTemp->succ;
    }
    return(pNodoTemp->data);
}

void Lista::mostrar(ostream& o) {
    // Completar
}
